import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class TelephoneTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		fail("Not yet implemented");
	}
	
	
	@Test
	public void testFormatting1() { //Tests to ensure string is formatted properly
		String number = Telephone.format("2225135555");
		
		assertEquals("(222)513-5555", number);
	}
	
	@Test
	public void testFormatting2() {  //Tests to ensure string is unformatted properly
		String number = Telephone.unformat("(222)513-5555");
		
		assertEquals("2225135565", number);
	}
	
	@Test
	public void testFormatting3() { //This will fail, its clearly not formatted correctly
		assertEquals(true, Telephone.isFormatted("((222))-513-2422."));
	}
}
