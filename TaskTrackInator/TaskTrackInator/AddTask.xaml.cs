﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TaskTrackInator
{
    /// <summary>
    /// Interaction logic for AddTask.xaml
    /// </summary>
    public partial class AddTask : Window
    {
        public AddTask()
        {
            InitializeComponent();
        }

        private MainWindow mainWindow = new MainWindow();

        public AddTask(Window callingWindow)
        {
            mainWindow = callingWindow as MainWindow;
            InitializeComponent();
        }

        public void populateTasks(MainWindow mainWindow)
        {
            List<Label> taskTitles = new List<Label>();
            List<TextBlock> taskDescs = new List<TextBlock>();

            //Add Task Titles
            {
                taskTitles.Add(mainWindow.tasktitle1);
                taskTitles.Add(mainWindow.tasktitle2);
                taskTitles.Add(mainWindow.tasktitle3);
                taskTitles.Add(mainWindow.tasktitle4);
                taskTitles.Add(mainWindow.tasktitle5);
                taskTitles.Add(mainWindow.tasktitle6);
                taskTitles.Add(mainWindow.tasktitle7);
                taskTitles.Add(mainWindow.tasktitle8);
                taskTitles.Add(mainWindow.tasktitle9);
            }

            //Add Task Descs
            {
                taskDescs.Add(mainWindow.taskdesc1);
                taskDescs.Add(mainWindow.taskdesc2);
                taskDescs.Add(mainWindow.taskdesc3);
                taskDescs.Add(mainWindow.taskdesc4);
                taskDescs.Add(mainWindow.taskdesc5);
                taskDescs.Add(mainWindow.taskdesc6);
                taskDescs.Add(mainWindow.taskdesc7);
                taskDescs.Add(mainWindow.taskdesc8);
                taskDescs.Add(mainWindow.taskdesc9);
            }

            //MessageBox.Show("Testing");

            mainWindow.tasktitle1.Content = "Testing";
            mainWindow.taskdesc1.Text = "Testing";

            if (mainWindow.tasktitle1.Content.ToString() == "" & mainWindow.taskdesc1.Text == "")
            {

            }
        }

        public void addTask(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Work in progress.");
            //populateTasks(this);
            
        }
    }
}