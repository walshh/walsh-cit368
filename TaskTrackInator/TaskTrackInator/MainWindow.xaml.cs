﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Windows;
using System.Data.SqlClient;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;

namespace TaskTrackInator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string dbConnection = ConfigurationManager.ConnectionStrings["db"].ConnectionString;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void loginRegisterButton(object sender, RoutedEventArgs e)
        {
            home.Visibility = Visibility.Hidden;
            register.Visibility = Visibility.Visible;
        }

        private void goToHome(object sender, RoutedEventArgs e)
        {
            home.Visibility = Visibility.Visible;
            register.Visibility = Visibility.Hidden;
        }

        private void openAddTaskWindow(object sender, RoutedEventArgs e)
        {
            AddTask addTask = new AddTask();

            addTask.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            addTask.Show();
            //this.close(); (CLOSES MAIN WINDOW, WE DONT WANT THIS)
        }


        private void registerAccount(object sender, RoutedEventArgs e)
        {
            //string dbConnection = ConfigurationManager.ConnectionStrings["aws"].ConnectionString;
            //string dbConnection = @"Data Source=cit386-01.cgq3zunatp5w.us-east-1.rds.amazonaws.com; Initial Catalog = tempdb; Integrated Security=True;";


            if (firstName.Text == "" || lastName.Text == "" || email.Text == "" || password.Text == "" || confirmPassword.Text == "")
                MessageBox.Show("Some fields are empty, try again");
            else if (password.Text != confirmPassword.Text)
                MessageBox.Show("Passwords do not match.");

            else
            {
                using (SqlConnection connection = new SqlConnection(dbConnection))
                {
                    String query = "INSERT INTO dbo.[Employee] (FirstName,LastName,Email,Password) VALUES (@FirstName,@LastName,@Email, @Password)";

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@firstName", firstName.Text.ToString());
                        command.Parameters.AddWithValue("@lastName", lastName.Text.ToString());
                        command.Parameters.AddWithValue("@email", email.Text.ToString());
                        command.Parameters.AddWithValue("@password", password.Text.ToString());

                        connection.Open();
                        int result = command.ExecuteNonQuery();
                        MessageBox.Show("Successful Registration.");
                        ClearRegistration();

                        // Check Error
                        if (result < 0)
                            Console.WriteLine("Error inserting data into Database!");
                    }
                }
            }
        }

       /* private void welcomeMessage()
        {
            using (SqlConnection newConnection = new SqlConnection(dbConnection))
            {
                string select = string.Format("SELECT FirstName, LastName FROM Employee;");
                //string select = "SELECT FirstName, LastName FROM [Employee]";
                SqlCommand sqlCommand = new SqlCommand(select, newConnection);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                //currentUser.Content = select.ToString();
                currentUser2.Content = "Welcome" + reader[1].ToString() + reader[2].ToString() + "!";
            }
        }*/
        private void ClearRegistration()
        {
            firstName.Text = "";
            lastName.Text = "";
            email.Text = "";
            password.Text = "";
            confirmPassword.Text = "";
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {

            string email = emailLogin.Text;
            string password = passLogin.Text;

            if (emailLogin.Text == "" || passLogin.Text == "")
            {
                MessageBox.Show("Please enter your email and password.");
            }
            else
            {
                using (SqlConnection newConnection = new SqlConnection(dbConnection))
                {
                    newConnection.Open();

                    string query = "SELECT FirstName, LastName FROM [Employee] WHERE [Email]=@email AND [Password]=@password";
                    SqlCommand sqlCommand = new SqlCommand(query, newConnection);

                    sqlCommand.Parameters.Add("email", SqlDbType.VarChar).Value = email;
                    sqlCommand.Parameters.Add("password", SqlDbType.VarChar).Value = password;

                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                    SqlDataReader reader = sqlCommand.ExecuteReader();
                                
                    while(reader.Read())
                    {
                        currentUser.Content = reader[0].ToString() + " " + reader[1].ToString() + "!";
                        //newConnection.Close();
                    }

                    MessageBox.Show("Successfully logged in.");
                    newConnection.Close();
                }


                register.Visibility = Visibility.Collapsed;
                home.Visibility = Visibility.Visible;


            }
        }

        private void openManageTaskWindow(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Work in Progress.");
        }

        private void openLocationAlertsWindow(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Work in Progress.");
        }
    }
}
